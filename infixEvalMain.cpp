

#include "OperatorStack.h"

#include "OperandStack.h"

using namespace std;


void executeOperation(OperatorStack &oprt,  OperandStack  &oprd, StackData e);


int main()

{

	OperatorStack oprt;  

	OperandStack  oprd;

	int operand;

	StackData d, e;

	bool parsingCurrentString;

	bool unaryFlag = true;

	char ch;

	cout << "Enter expression & hit <Enter>: "; //get single expression from user

	cout << endl;

	parsingCurrentString = true;

	while (parsingCurrentString) // while we are scanning a single expression

	{

		ch = cin.peek();   // peeks at io stream, similar to out peek with a stack

		if (isdigit(ch))   // if the character is a digit,

		{

			unaryFlag = false;

			cin >> operand;  // get the whole integer

			oprd.push(operand); // push it on the operand stack

		}

		else                        // next character not an operand

		{

			ch = cin.get();


			if (ch == '(')

			{                                              // case of left parenthesis

				unaryFlag = true;

				d.op = '(';

				d.precedence = 20;

				oprt.push(d);

			}

			else if (ch == ' ' || ch == '\t')             // case of white space

			{

				

			}

			else if (ch == ')')                           // case of right parenthesis

			{

				if (oprt.isEmpty())

				{

					cout << "Invalid syntax" << endl << endl;

					return 0;

				}

				else

				{

					e = oprt.peek();

					while (e.precedence != 20)

					{

						executeOperation(oprt, oprd, e);

						if (!oprt.isEmpty())

						{

							e = oprt.peek();

						}

					}

					oprt.pop();

				}

			}

			else if (ch =='+' || ch == '-' || ch == '*'   //case of an operator

					|| ch == '/' || ch == '%')

			{

				if (ch == '+')

				{

					d.op = '+';

					d.precedence = 5;

				}

				if (ch == '-')

				{

					if (unaryFlag)

					{

						d.op = '-';

						d.precedence = 15;

					}

					else

					{

						d.op = '-';

						d.precedence = 5;

					}

				}

				if (ch == '*')

				{

					d.op = '*';

					d.precedence = 10;

				}

				if (ch == '/')

				{

					d.op = '/';

					d.precedence = 10;

				}

				if (ch == '%')

				{

					d.op = '%';

					d.precedence = 10;

				}

				if (oprt.isEmpty())

				{

					oprt.push(d);

				}

				else

				{

					e = oprt.peek();

					///////////////////adjusted this while loop to be simpler and to keep from bypassing ('s

					while ((d.precedence <= e.precedence) && (!oprt.isEmpty()) && e.precedence != 20)

					{

						executeOperation(oprt, oprd, e);

						if (!oprt.isEmpty())

						{

							e = oprt.peek();////////////////moved to after function and + empty check

						}                              

					}

					oprt.push(d);			

				}

			}

			else if (ch == '\n')                         // case of end of input string

			{

				while (!oprt.isEmpty())

				{

					e = oprt.peek();

					executeOperation(oprt, oprd, e);

				}

				parsingCurrentString = false;

				cout << "End of string reached" << endl;

				cout << "Answer = " << oprd.peek() << endl << endl;

			}


			else

			{

				cout << "Invalid character encountered" << endl << endl; //(1+(-1)*(5-20/3)

				return 0;

			}


		}    // end of "else not an operand"

	//oprt.printStack();  // for debugging purposes

	//oprd.printStack();  // for debugging purposes

	//cout << "next" << endl << endl;

	} // while loop


	//oprt.printStack();  // for debugging purposes

	//oprd.printStack();  // for debugging purposes


	return 0;

}


void executeOperation(OperatorStack &oprt,  OperandStack &oprd, StackData e)

{	

	int result, operand1, operand2;

	if ((e.op == '-') && (e.precedence == 15))

	{

		operand1 = oprd.pop();

		result = -1 * operand1;

		cout << "-1 * " << operand1 << " = " << result << endl;

	}

	else

	{

		operand2 = oprd.pop();

		operand1 = oprd.pop();

		if (e.op == '+')

		{

			result = operand1 + operand2;

			cout << operand1  << " + " << operand2 << " = " << result << endl;

		}

		else if (e.op == '-')

		{

			result = operand1 - operand2;

			cout << operand1  << " - " << operand2 << " = " << result << endl;

		}

		else if (e.op == '*')

		{

			result = operand1 * operand2;

			cout << operand1  << " * " << operand2 << " = " << result << endl;

		}

		else if (e.op == '/')

		{

			result = operand1 / operand2;

			cout << operand1  << " / " << operand2 << " = " << result << endl;

		}

		else if (e.op == '%')

		{

			result = operand1 % operand2;

			cout << operand1  << " % " << operand2 << " = " << result << endl;

		}

	}

	oprd.push(result);

	oprt.pop();

	


} // end exOp

