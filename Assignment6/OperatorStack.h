/******************************************************************
                                                               
       Class "operator stack" inplemented with pointers          
                                                            
******************************************************************/

#ifndef OPERATOR_STACK_H
#define OPERATOR_STACK_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cctype>
#include <string>
using namespace std;

struct StackData
{
	char op;
	int  precedence;
};

struct OperatorStackNode
{
	StackData data;
	OperatorStackNode *next;
};

typedef OperatorStackNode* operatorNodePtr;


class OperatorStack
{
	private:

		int      numElements;
		operatorNodePtr  top;

	public:

		OperatorStack ()
		{  numElements = 0; top = NULL;  }


		bool isEmpty ()
		{  return (numElements == 0);  }


		void printStack ()
		{
			int count = 0;
			cout << "\n   Dump Stack Contents in Order \n\n";
			operatorNodePtr p = top;
			while (p != NULL)
			{
			count++;
			cout << setw(4) << right << count << ".  ";
			cout << "Operator: " << p->data.op << "   precedence: " << p->data.precedence <<  endl; 
			p = p->next;
			}
			if (numElements == 0)
			cout << "\n      Stack is empty \n\n";
			cout << endl;
		}

		void push (StackData dataInput)
		{
			operatorNodePtr   p;
			p = new OperatorStackNode;  // default constructor
			p->data = dataInput;
			numElements++;
			p->next = top;  // push it on the top !!
			top = p;
			} 

		StackData pop ()
		{
			StackData returnData;
			operatorNodePtr p;
			numElements--;
			p = top;
			top = p->next;
			returnData = p->data;
			delete p;
			return returnData;
		}

		StackData peek ()
		{
			StackData returnData;
			returnData = top->data;            
			return returnData;
		}

};
#endif

