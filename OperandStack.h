/******************************************************************
                                                                
		Class "operand stack" implemented with pointers 
                                                                
******************************************************************/

#ifndef OPERAND_STACK_H
#define OPERAND_STACK_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cctype>
#include <string>
using namespace std;

struct OperandStackNode
{
	int value;
	OperandStackNode *next;
};

typedef OperandStackNode* operandNodePtr;


class OperandStack
{
	private:

		int      numElements;
		operandNodePtr  top;

	public:

		OperandStack ()
		{  numElements = 0; top = NULL;  }


		bool isEmpty ()
		{  return (numElements == 0);  }


		void printStack ()
		{
			int count = 0;
			cout << "\n   Dump Stack Contents in Order \n\n";
			operandNodePtr p = top;
			while (p != NULL)
			{
			count++;
			cout << setw(4) << right << count << ".  ";
			cout << setw(10) << right  << p->value << endl << endl; 
			p = p->next;
			}
			if (numElements == 0)
			cout << "\n      Stack is empty \n\n";
		}

		void push (int valueInput)
		{
			operandNodePtr   p;
			p = new OperandStackNode;  // default constructor
			p->value = valueInput;
			numElements++;
			p->next = top;  // push it on the top !!
			top = p;
		} 

		int pop ()
		{
			int  valueToBeReturned;
			operandNodePtr p;
			numElements--;
			p = top;
			top = p->next;
			valueToBeReturned = p->value;
			delete p;
			return valueToBeReturned;
		}

		int peek ()
		{            
			return top->value;
		}

};
#endif

