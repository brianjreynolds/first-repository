//This is the implementation for the college.h
//This class has ability to add,remove,display et al.
//Author: Chuxian Wang
//Course: CS2401

#include "node.h"
#include "course.h"
#include "college.h"
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

//Constructor:
	College::College(){
	    head = NULL;
        }

	College::College(string& astudent){
		head = NULL;
	   student = astudent;
	}

//Big 3

//Destructor
	College::~College(){
		node * rmptr;
		while(head != NULL){
			rmptr = head;
			head = head -> link();
			delete rmptr;
			}
				}

//copy constructor
	College::College(const College&other){
		const node * cursor;
		node * tail;
		if(other.head == NULL)
			head = NULL;
		else{
			head = new node(other.head -> data());
			tail = head;
			cursor = other.head->link();
			while(cursor != NULL){
				tail -> set_link(new node(cursor->data()));
			//	tail -> set_data(other.head->data());
				tail = tail->link();
				cursor = cursor->link();
					}//while
				}//else
	}//function


//Assignment operator
	void College::operator = (const College& other){
		const node*cursor;
		node* tail;

		if(&other == this)
		return; //self check
		
		else{
		if(other.head != NULL){
 			head = new node(other.head -> data());//copy
			tail = head;
			cursor = other.head->link();
			while(cursor != NULL){
				tail -> set_link(new node(cursor -> data()));
				//tail -> set_data(other.head->data());
				tail = tail ->link();
				cursor = cursor -> link();
					}//while
			}//if
			}//else
}//Funciton
				



void College::add(course& classname){
		node* addptr;

		if(head == NULL || head -> data().get_course_number() >classname.get_course_number()){
			head = new node;
			head -> set_data(classname); //set_data
				}
		else{	
			addptr = head;
		while(addptr->link()!= NULL && addptr->link() ->data().get_course_number() < classname.get_course_number()){
			addptr = addptr->link();}
			addptr-> set_link(new node(classname, addptr->link()));
		}//else
			}//function


void College::remove(string& name){

		node * Remove;
		Remove = head->link();
		node* previous;
		previous = head;
		
		if(head == NULL){
		cout << "Empty list\n";
		return;}

		else if(head->data().get_course_number() == name){
			node*temp;
			temp = head;
			head = head->link();
			delete temp;}


		else{
		
		while(Remove != NULL){
			if(Remove -> data().get_course_number() == name){
				previous -> set_link(Remove->link());
				delete Remove;
				return;
				}//if
			else{
					previous = previous -> link();
					Remove = Remove->link();
					}//else
				
			}//while
			}//else
		if(Remove == NULL)
			cout << "This class is not in the list\n";


}//Function


//caculate the total Credit hours
double College::hours(){
	double total_hours=0.0;
	node*cursor;
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(1);
	if(head == NULL){
	return(0.0); //no class
			}
	else{
	cursor = head;
	while(cursor != NULL){
		total_hours+=cursor->data().get_hours();
		cursor = cursor->link();
			}//while
		return total_hours;
			}//else

		}

//caculate tehe GPA
double College::gpa(){
	node*cursor;
	double point = 0.00;
	double whole_point = 0.00;
	double T_hours = 0.00;
	double gpa = 0.00;
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	if(head == NULL){
		return 0.0; //No class
		}
	else{
		cursor = head;
		while (cursor !=NULL){
			T_hours+=cursor->data().get_hours();
			point = cursor->data().get_hours() * cursor->data().get_number_grade();
			whole_point +=point;
			cursor = cursor -> link();
			}
			gpa = whole_point / T_hours;
			return gpa;;
			}//else
}//Function

//display on the screen
void College::display(ostream& outs)const{
	if(head == NULL){
		outs << "Empty list\n";
		return;
			}
	else{
		node* cursor;
		cursor = head;
		while(cursor != NULL){
		outs << cursor->data() << endl;
		cursor = cursor -> link();
			}
		}//else

}//function


//Save the data of courses in file
void College::save(ostream& ou){
	if(head == NULL){
	cout << "Empty list.\n";
	return;}
	else{
	node * cursor;
	cursor = head;
	while(cursor != NULL){
	ou << cursor->data();
	cursor = cursor -> link();}
		}	
		}

//load data from file to the list
void College::load(istream& in){
	node * cursor;
	course coursename;
	while(in >> coursename){
	if(head == NULL){
	head = new node;
	head -> set_data(coursename);
	head -> set_link(NULL);
		}
	else{
		for(cursor = head; cursor -> link() != NULL; cursor = cursor -> link());
		cursor -> set_link(new node);
		cursor = cursor->link();
		cursor ->set_data(coursename);
		cursor ->set_link(NULL);
				}
			}//while
			}//Fcuntion
