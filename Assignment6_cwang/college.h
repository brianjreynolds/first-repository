//This is the college class as contianer class
//to oder the items in the list
//Author: Chuxian Wang
#ifndef COLLEGE_H
#define COLLEGE_H
#include <iostream>
//#include "college.h"
#include "course.h"
#include "node.h"
using namespace std;

class College{

	public:	
	//Constructor
 	College();
	College(string& student);


	//big 3
	~College();
	College(const College& other);
	void operator = (const College& other);
	

	//member functions
	void add(course& classname);
	void display(ostream& outs)const;
	void remove(string& name);
	double hours();
	double gpa();
	void load(istream& in);
	void save(ostream& ou);


	string get_student()const{return student;}



	private:
	string student;
	node* head;
		};

#endif
